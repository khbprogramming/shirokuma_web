
          var $c = $('#card1'),
open = false;

$c = {
  root: $c,
  bg: $c.find('.card__background1'),
  status: $c.find('.card__status1'),
  brand: $c.find('.card__brand1'),
  content: $c.find('.card__content1'),
  timeline: $c.find('.event__timeline1') };


var toggleCard1 = function () {

  open = !open;

  var rW = $c.root.innerWidth(),
  rH = $c.root.innerHeight(),
  cW = $c.content.innerWidth(),
  cH = $c.content.innerHeight();

  TweenMax1.
  to($c.bg, .6, {
    width: open ? cW : rW,
    height: open ? cH : rH,
    ease: Elastic.easeOut.config(1, .5) });


  TweenMax1.
  to($c.brand, .6, {
    y: open ? 0 - (cH - rH) / 2 : 0,
    force3D: true,
    ease: Elastic.easeOut.config(1, .5) });


  TweenMax1.
  to($c.status, open ? .07 : .3, {
    autoAlpha: open ? 0 : 1,
    force3D: true,
    delay: open ? 0 : .25,
    ease: Expo.easeOut });


  if (open) {

    TweenMax1.
    fromTo($c.content, .6, {
      scale: .6 },
    {
      scale: 1,
      force3D: true,
      ease: Elastic.easeOut.config(1, .5) });


  }

  TweenMax1.
  fromTo($c.content, open ? .4 : .1, {
    autoAlpha: 1 },
  {
    autoAlpha: open ? 1 : 0,
    ease: Cubic.easeOut });


  var $events = $c.timeline.find('li1 li1');

  if (open) {

    TweenMax1.
    staggerFromTo($events, .5, {
      y: -40 },
    {
      y: 0,
      delay: .15,
      force3D: true,
      ease: Elastic.easeOut.config(1, .5) },
    .05);

    TweenMax1.
    staggerFromTo($events, .45, {
      opacity: 0 },
    {
      opacity: 1,
      delay: .15,
      ease: Cubic.easeOut },
    .05);

    var $dates = $c.timeline.find('.event__group-date1');

    TweenMax1.
    staggerFromTo($dates, .5, {
      y: -40 },
    {
      y: 0,
      delay: .15,
      force3D: true,
      ease: Elastic.easeOut.config(1, .5) },
    .05);

    TweenMax1.
    staggerFromTo($dates, .45, {
      opacity: 0 },
    {
      delay: .15,
      opacity: 1,
      ease: Cubic.easeOut },
    .05);

  }

};

$c.root.on('click', toggleCard1);
             