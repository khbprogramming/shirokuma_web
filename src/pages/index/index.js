import React from "react";
import { lastAdded } from "../../querys/query";
import Episode_card from "./components/episode_card/index";
import Songs from "./components/musics";

const Index = () => {
  return (
    <>
      <div className="home">
        {/* <div className="home__background"></div> */}
        <h1 className="home__title  ">Сүүлд нэмэгдсэн</h1>
        <div className="home__last-added">
          {lastAdded.map((item, index) => (
            <Episode_card item={item} key={index} />
          ))}
        </div>
        <h1 className="home__title  ">Бүрэн гарсан</h1>
        <div className="home__last-added">
          {lastAdded.map((item, index) => (
            <Episode_card item={item} key={index} />
          ))}
        </div>
      </div>
      <Songs />
    </>
  );
};

export default Index;
