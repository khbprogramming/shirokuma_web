import React, { useState } from "react";
import Slide from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { songsData as data } from "../../../../querys/query";

const Index = () => {
  const [item, setItem] = useState(data[0]);
  let slickGoto;
  return (
    <div className="home__songs">
      <h1 className="home__title">Дуу</h1>
      <div className="home__songs-container">
        <div className="home__songs-slide">
          <video
            className="home__songs-container_bigVideo"
            src={item[360]}
            controls
            poster="https://i.pinimg.com/474x/da/3a/c1/da3ac175d1bbd31882c67d80f9260abe.jpg"
          />
        </div>

        <div className="home__songs-slide-list">
          <Slide
            ref={(evt) => (slickGoto = evt)}
            className="home__songs-slide-list_slide"
            arrows={false}
            vertical={true}
            slidesToShow={5}
            verticalSwiping={true}
            // infinite={false}
            // beforeChange={(current, next) => {
            //   setItem(data[next]);
            // }}
          >
            {data.map((item, index) => (
              <div
                key={index}
                onClick={() => {
                  slickGoto.slickGoTo(index);
                  setItem(item);
                }}
              >
                <h1>{item.name}</h1>
              </div>
            ))}
          </Slide>
        </div>
      </div>
    </div>
  );
};

export default Index;
