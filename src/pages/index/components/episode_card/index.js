import React, { useState } from "react";
import { Link } from "react-router-dom";

const Index = (props) => {
  const [loaded, setLoaded] = useState("false");
  return (
    <Link to={`/watch/${123}`}>
      <div className="card-item">
        <img
          className={"loaded-" + loaded}
          onLoad={() => {
            setLoaded(true);
          }}
          src={"./files/images/animePoster/" + props.item.src}
          alt={props.item.name}
        />
        <span className="card-item__episode">{props.item.episode}-р анги</span>
        <div className="card-item__name">
          <h3>{props.item.name}</h3>
        </div>
      </div>
    </Link>
  );
};

export default Index;
