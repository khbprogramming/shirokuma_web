export const lastAdded = [
  {
    src: "image1.jpg",
    episode: 21,
    name: "Steins gate",
  },
  {
    src: "image2.jpg",
    episode: 13,
    name: "Kill la kill",
  },
  {
    src: "image3.jpg",
    episode: 12,
    name: "Nanatsu no taiza",
  },
  {
    src: "image4.jpg",
    episode: 5,
    name: "Domestic kanojo",
  },
  {
    src: "image5.png",
    episode: 28,
    name: "Shirome",
  },
  {
    src: "image6.png",
    episode: 53,
    name: "Shigatsu wa kimi no uso",
  },
  {
    src: "image7.jpg",
    episode: 3,
    name: "Juuni taisen",
  },
  {
    src: "image8.jpg",
    episode: 15,
    name: "Tabau mother",
  },
  {
    src: "image7.jpg",
    episode: 3,
    name: "Juuni taisen",
  },
];

export const songsData = [
  {
    name: "Osomatsu",
    360: "https://dreamer.mn/shirokuma/osomatsu/osomatsu_360.mp4",
    540: "https://dreamer.mn/shirokuma/osomatsu/osomatsu_540.mp4",
    720: "https://dreamer.mn/shirokuma/osomatsu/osomatsu_720.mp4",
  },
  {
    name: "Juuni taisen",
    360: "https://dreamer.mn/shirokuma/juuni/01_360.mp4",
    540: "https://dreamer.mn/shirokuma/juuni/01_540.mp4",
    720: "https://dreamer.mn/shirokuma/juuni/01_720.mp4",
  },
  {
    name: "WWW.working",
    360: "https://dreamer.mn/shirokuma/Www_working/Episode_01_360.mp4",
    540: "https://dreamer.mn/shirokuma/Www_working/Episode_01_540.mp4",
    720: "https://dreamer.mn/shirokuma/Www_working/Episode_01_720.mp4",
  },
  {
    name: "Usagi drop",
    360: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
    540: "https://dreamer.mn/shirokuma/Usagi_Drop/01_540.mp4",
    720: "https://dreamer.mn/shirokuma/Usagi_Drop/01_720.mp4",
  },
  {
    name: "Nanaka drop",
    360: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
    540: "https://dreamer.mn/shirokuma/Usagi_Drop/01_540.mp4",
    720: "https://dreamer.mn/shirokuma/Usagi_Drop/01_720.mp4",
  },
  {
    name: "Hi drop",
    360: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
    540: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
    720: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
  },
  {
    name: "Pubg drop",
    360: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
    540: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
    720: "https://dreamer.mn/shirokuma/Usagi_Drop/01_360.mp4",
  },
];
