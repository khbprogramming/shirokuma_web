import React from "react";
import { Link } from "react-router-dom";

const Menu = () => {
  return (
    <div className="menu">
      <Link to="/">
        <img
          src="/files/images/logo.png"
          className="menu__logo"
          alt="Shirokuma logo"
        />
      </Link>
      <ul>
        <Link to="/">
          <li className="active">Үндсэн</li>
        </Link>
        <Link to="/Season">
          <li>Цуврал</li>
        </Link>
        <Link to="/Movie">
          <li>Кино</li>
        </Link>
        <Link to="/News">
          <li>Мэдээ</li>
        </Link>
        <Link to="/about">
          <li>Бидний тухай</li>
        </Link>
        <Link to="/joinup">
          <li>+Нэгдэх</li>
        </Link>
      </ul>
    </div>
  );
};

export default Menu;
