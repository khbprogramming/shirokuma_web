import React from "react";
import Home from "./pages/index";
import Menu from "./components/menu";
import Watch from "./pages/watch/watch";
import "./modules/styles/styles.scss";
import "normalize.css";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  // Redirect,
} from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <Menu />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/" component={Watch} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
